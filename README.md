# gitlab-ci-tools

Base templates and functionality to support shared feature needs in GitLab CI/CD such as building/pushing images, and managing deploys with helm

## Renovate Presets

These are presets which can be used for renovate configurations in Gitlab.

### renovate-alpine-docker.json

This allows Renovate to update container image versions for programming languages using the Alpine architecture, such as
Ruby and Golang. The existing Renovate logic to determine upgrades will never make the following upgrade:

`3.3.1-alpine3.19` -> `3.3.2-alpine3.20`

In order to use, follow the [Gitlab config syntax][gitlab-renovate-preset], such as:

```json
  extends: [
      "base:recommended",
      "gitlab>ucsdlibrary/development/gitlab-ci-tools:renovate-alpine-docker"
  ],
```


## gitlab-ci-renovate-lint.yml

This template defines a `renovate:lint` job which is appended to the `lint` stage of a pipeline.

```yaml
include:
- project: 'ucsdlibrary/development/gitlab-ci-tools'
  ref: trunk
  file: 'gitlab-ci-renovate-lint.yml'
```

## gitlab-ci-kaniko-build.yml

This template defines a `build` job using the [kaniko][kaniko] builder image. We
use Kaniko because, unlike Docker (at present), it can run in userspace without
root permissions. This allows us to run our GitLab runners without root access.

It also leverages the kaniko `cache` option, so you will see an additional
`cache` entry in your GitLab registry. This greatly improves build times once
the layers are cached.

If you want to run a pipeline that does _not_ use the kaniko `cache`, you can invoke a pipeline and pass an environment
variable called `CI_BUILD_NO_CACHE`, with a value of anything you choose. Perhaps something like "yes". This can be
helpful if you need to force a new image being built when the kaniko cache is using cache layers when it shouldn't, for
example updating system packages.

*Note*: This `build` job does assume that your `Dockerfile` has a [named stage][multistage] of `production` that corresponds to the final "run" image you
need for your application.

## gitlab-ci-helm-deploy.yml

This template defines the following jobs for the `deploy` stage:
* review
* cleanup
* staging
* production
* rollback

Notes:
* Your Helm chart directory MUST be named `chart`.
* You MUST have the `KUBE_ROLE_CONFIG_PRODUCTION` and `KUBE_NAMESPACE_REVIEW`
* variables defined. This is the `base64`-encoded
    `kubeconfig` file for the application role account, which should have access
    to deploy to your application Rancher Project, and the namespaces within it.
* Your `chart/template/deployment.yaml` MUST reference the image tag via
    `Values.image.tag` as opposed to the default `Chart.appVersion`.
    ```
    image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"
    ```

## gitlab-ci-security.yml

This template exposes the Gitlab Security features that we're currently
leveraging such as container and dependency scanning. It is meant to be a single
source of truth for how these scans are fun for all adopting applications.

Note, in order to leverage this, it is expected that the following are setup:

* A schedule that sets the `UCSD_SECURITY_PIPELINE` variable (this variable is
    used to run security scanning jobs)
* A CI/CD snippet added to the project `.gitlab-ci.yml` file such as:

```yaml
include:
- project: 'ucsdlibrary/development/gitlab-ci-tools'
  ref: trunk
  file: 'gitlab-ci-security.yml'
```


### Production Job

The following environment variables are supported/required:

Notes:
* Your tls secret MUST have `tls-secret` as its `metadata` `name`

| Name                                  | Description                                                             | Required          |
| ------------------------------------- | ----------------------------------------------------------------------- | ----------------- |
| `APP_HOST_PRODUCTION`                 | URL hostname for application                                            | Yes               |
| `HELM_INITIAL_VALUES_FILE_PRODUCTION` | `base64`-encoded Helm values file, only used on initial release         | No                |
| `HELM_VALUES_FILE_PRODUCTION`         | `base64`-encoded Helm values file to override defaults in `values.yaml` | Yes               |
| `INGRESS_PATH`                        | Custom relative path for application ingress.                           | No (default: `/`) |
| `KUBE_NAMESPACE_PRODUCTION`           | Kubernetes `namespace` that the application will deploy to              | Yes               |
| `TLS_CRT`                             | TLS `crt` portion of the keypair. Used for ingress and TLS secret       | Yes               |
| `TLS_KEY`                             | TLS `key` portion of the keypair. Used for ingress and TLS secret       | Yes               |

### Staging Job

The following environment variables are supported/required:

Notes:
* Your tls secret MUST have `tls-secret` as its `metadata` `name`

| Name                               | Description                                                             | Required          |
| ---------------------------------- | ----------------------------------------------------------------------- | ----------------- |
| `APP_HOST_STAGING`                 | URL hostname for application                                            | Yes               |
| `HELM_INITIAL_VALUES_FILE_STAGING` | `base64`-encoded Helm values file, only used on initial release         | No                |
| `HELM_VALUES_FILE_STAGING`         | `base64`-encoded Helm values file to override defaults in `values.yaml` | Yes               |
| `INGRESS_PATH`                     | Custom relative path for application ingress.                           | No (default: `/`) |
| `KUBE_NAMESPACE_STAGING`           | Kubernetes `namespace` that the application will deploy to              | Yes               |
| `TLS_CRT`                          | TLS `crt` portion of the keypair. Used for ingress and TLS secret       | Yes               |
| `TLS_KEY`                          | TLS `key` portion of the keypair. Used for ingress and TLS secret       | Yes               |

### Review Job

The following environment variables are supported/required:

Notes:
* You MUST have `REVIEW_DOMAIN` set for review jobs to properly setup wildcard-style ingress.

| Name                               | Description                                                             | Required          |
| ---------------------------------- | ----------------------------------------------------------------------- | ----------------- |
| `HELM_INITIAL_VALUES_FILE_REVIEW`  | `base64`-encoded Helm values file, only used on initial release         | No                |
| `HELM_VALUES_FILE_REVIEW`          | `base64`-encoded Helm values file to override defaults in `values.yaml` | Yes               |
| `INGRESS_PATH`                     | Custom relative path for application ingress.                           | No (default: `/`) |
| `KUBE_NAMESPACE_REVIEW`            | Kubernetes `namespace` that the application will deploy to              | Yes               |

### Cleanup Job

This job is responsible for cleaning up and removing `review` job deployments.
In general, this should "just work".

Note: The `delete_stateful_set_pvcs` is called because some chart dependencies
that define `StatefulSet` volume claims will NOT be removed with a normal `helm
delete` command. The function currently targets the `release` label. Which the
PostgreSQL chart supports. In the future, we may need to make this more flexible
for other charts.

[gitlab-renovate-preset]:https://docs.renovatebot.com/config-presets/#gitlab
[kaniko]:https://github.com/GoogleContainerTools/kaniko
[multistage]:https://docs.docker.com/develop/develop-images/multistage-build/#name-your-build-stages
